.PHONY: pdf clean

SVG := $(wildcard svg/*.svg)
PDF := $(SVG:svg/%.svg=pdf/%.pdf)
PNG := $(SVG:svg/%.svg=png/%.png)

# PDF from SVG
pdf/%.pdf: svg/%.svg
	inkscape $< --export-pdf=$@

# PNG from SVG
png/%.png: svg/%.svg
	inkscape $< --export-png=$@ --export-dpi=300

all: pdf png

# All PDF files
pdf: $(PDF)

# All PDF files
png: $(PNG)

clean:
	rm -f pdf/*.pdf png/*.png
