# Graphical Depiction of Study Design

Graphical depictions of study designs based on [this article by Schneeweiss et al.](https://annals.org/aim/article-abstract/2728197/graphical-depiction-longitudinal-study-designs-health-care-databases)

## Figures

[Figure 2. Exposure-based cohort entry where the cohort entry date is selected prior to application of exclusion criteria](svg/figure_2.svg)

![Figure 2](png/figure_2.png)

## How to use

1. Clone this repository
2. Use the SVG file as a template to create your own figures
3. Convert to PDF and/or PNG with [Inkscape](https://inkscape.org/), either manually or using the Makefile
